const cal = (...arr) => {
  let sum = 0;
  arr = arr[0];
  arr.forEach((e) => {
    sum += e;
  });
  return (sum / arr.length).toFixed(2);
};

//khối lớp 1
document.querySelector("#btnKhoi1").addEventListener("click", () => {
  let inputs = Array.from(document.querySelectorAll(".khoi1 input"));
  let scores = inputs.map((e) => {
    return e.value * 1;
  });
  document.querySelector("#tbKhoi1").innerHTML = cal(scores);
});

//khối lớp 2
document.querySelector("#btnKhoi2").addEventListener("click", () => {
  let inputs = Array.from(document.querySelectorAll(".khoi2 input"));
  let scores = inputs.map((e) => {
    return e.value * 1;
  });
  document.querySelector("#tbKhoi2").innerHTML = cal(scores);
});
