const text = "Hover Me!";
const spread = [...text];

let contentHTML = "";
spread.forEach((element) => {
  contentHTML += `<span class="txt">${element}</span>`;
});

document.querySelector(".heading").innerHTML = contentHTML;
