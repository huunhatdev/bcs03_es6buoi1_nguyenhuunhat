const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let colorNow = colorList[0];

let contentHTML = "";
colorList.forEach((e, i) => {
  contentHTML += `<button class="color-button ${e} ${
    e === colorList[0] ? "active" : ""
  }" onclick="changeColor('${e}')"></button>`;
});
document.querySelector("#colorContainer").innerHTML = contentHTML;

const removeActive = () => {
  let button = document.querySelector(".color-button.active");
  if (button) {
    button.classList.remove("active");
  }
};

const removeColorNow = () => {
  let e = document.querySelector(`#house`);
  if (colorNow) e.classList.remove(colorNow);
};

const changeColor = (color) => {
  removeActive();
  removeColorNow();
  let element = event.srcElement;
  element.classList.add("active");
  colorNow = color;
  document.querySelector("#house").classList.add(color);
};
